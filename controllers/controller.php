<?php


require_once('./models/pelis_model.php');
require_once('./vista/PelisVista.php');


class Controller
{
    private $pelisModel;
    private $pelisVista;
    function __construct()
    {
        $this->pelisModel = new PelisModel();
        $this->pelisVista = new PelisVista();
    }

    function mostrarGenero($genero)
    {
        $pelisGenero = $this->pelisModel->obtenerPeliculasXGenero($genero);
        $this->pelisVista->mostrarXGenero($pelisGenero);
    }

    function agregarPeli()
    {
        $nombre = $_REQUEST['nombre'];
        $duracion = $_REQUEST['duracion'];
        $anio = $_REQUEST['anio'];
        $genero = $_REQUEST['genero'];


      $this->pelisModel->agregarPelicula($nombre, $duracion, $anio, $genero);

        header("Location: " . BASE_URL);
    }
    function mostrarPeliculas()
    {

        $peliculas = $this->pelisModel->mostrarTodasPeliculas();
        $this->pelisVista->mostrarPeliculas($peliculas);

    }

    function borrarPeli($id)
    {
       $this->pelisModel->eliminarPelicula($id);
       header("Location: " . BASE_URL);
    }

    function editarPeli($id)
    {
       $peliEditar = $this->pelisModel->obtenerPeliAEditar($id);
       $this->pelisVista->mostrarFormEditar($peliEditar);
    }

    function confirmarEdicion($id){
        $nombre = $_REQUEST['nombre'];
        $duracion = $_REQUEST['duracion'];
        $anio = $_REQUEST['anio'];
        $genero = $_REQUEST['genero'];

        $this->pelisModel->edicionConfirmada($id,$nombre,$duracion,$anio,$genero);
        header("Location: " . BASE_URL);

    }


    function mostrarCalificacion($calificacion)
    {
       $pelisCalif =$this->pelisModel->mostrarPeliculasXCalif($calificacion);
       $this->pelisVista->mostrarXCalificacion($pelisCalif);

    }
    function agregarGenero(){
        $this->pelisVista->mostrarFormNuevoGenero();
    }
    function confirmaGeneroNuevo(){
        $nombre = $_REQUEST['nombre'];
        $this->pelisModel->agregarNuevoGenero($nombre);
        header("Location: " . BASE_URL);

    }
    function mostrarListaGeneros(){
        $generos = $this->pelisModel->obtenerGeneros();
        $this->pelisVista->listarGeneros($generos);
    }
    function borrarGenero(){
        $generos = $this->pelisModel->obtenerGeneros();
        $this->pelisVista->mostrarGeneros($generos);
    }

    function eliminarGenero($id){
        $this->pelisModel->generoEliminado($id);
        header("Location: " . BASE_URL);

        
    }
}
