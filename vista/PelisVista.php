<?php

require_once('./libs/smarty-3.1.39/libs/Smarty.class.php');

class PelisVista
{
    function mostrarXGenero($pelisGenero)
    {

        $smarty = new Smarty();
        $smarty->assign('peliculas', $pelisGenero);

        $smarty->display('lista.tpl');
    }

    function mostrarPeliculas($peliculas){
        $smarty = new Smarty();
        $smarty->assign('peliculas', $peliculas);

        $smarty->display('lista.tpl');
        


    }
    function mostrarXCalificacion($calificacion){

        $smarty = new Smarty();
        $smarty->assign('peliculas', $calificacion);

        $smarty->display('lista.tpl');
    }

    function mostrarFormEditar($peliEditar){
        $smarty = new Smarty();
        $smarty->assign('pelicula', $peliEditar);

        $smarty->display('formEditarPeli.tpl');

    }
    function mostrarFormNuevoGenero(){
        $smarty = new Smarty();
        $smarty->display('formAgregarGenero.tpl');
    }
    function listarGeneros($generos){
        $smarty = new Smarty();
        $smarty->assign('generos', $generos);
        $smarty->display('header.tpl');
        
    }
    function mostrarGeneros($generos){
        $smarty = new Smarty();
        $smarty->assign('generos', $generos);
        $smarty->display('generos.tpl');
        

    }
}

