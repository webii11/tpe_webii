-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-10-2021 a las 22:25:01
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `barba_negra`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_genero` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_genero`, `nombre`) VALUES
(1, 'accion'),
(2, 'comedia'),
(5, 'terror');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelicula`
--

CREATE TABLE `pelicula` (
  `id_pelicula` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `duracion` int(11) NOT NULL,
  `calificacion` decimal(10,0) NOT NULL DEFAULT 0,
  `anio` int(11) NOT NULL,
  `genero_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pelicula`
--

INSERT INTO `pelicula` (`id_pelicula`, `nombre`, `duracion`, `calificacion`, `anio`, `genero_id`) VALUES
(0, 'Injustice', 78, '4', 2021, 1),
(4, 'Venom 2: Habrá Matanza', 97, '4', 2021, 1),
(5, 'Sin tiempo para morir', 163, '4', 2021, 1),
(6, 'Rapidos y furiosos 9', 145, '4', 2021, 1),
(7, 'Godzilla vs Kong', 113, '1', 2021, 1),
(8, 'Kickboxer: Venganza', 90, '3', 2016, 1),
(9, 'Boost', 99, '1', 2016, 1),
(17, 'Amigos con Ventaja', 120, '2', 2014, 2),
(18, 'Buenos vecinos', 89, '3', 2017, 2),
(19, 'Dos buenos tipos', 116, '3', 2016, 2),
(20, 'Hotel Transylvania', 91, '4', 2012, 2),
(21, 'El retiro', 90, '1', 2019, 2),
(22, 'Soul', 106, '5', 2020, 2),
(23, 'Te Quiero Como Amigo', 110, '1', 2021, 2),
(31, 'Finale', 100, '3', 2018, 5),
(33, 'Enjaulado', 81, '1', 2021, 5),
(34, 'El manicomio – La cuna del terror', 123, '2', 2018, 5),
(35, 'Dark Light', 90, '4', 2019, 5),
(36, 'La bruja del bosque', 92, '3', 2017, 5),
(37, 'Ouija Warehouse', 86, '2', 2021, 5),
(38, 'Posesión Diabólica', 98, '1', 2020, 5);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_genero`);

--
-- Indices de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD PRIMARY KEY (`id_pelicula`),
  ADD KEY `genero_id` (`genero_id`),
  ADD KEY `genero_id_2` (`genero_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_genero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  MODIFY `id_pelicula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD CONSTRAINT `pelicula_ibfk_1` FOREIGN KEY (`genero_id`) REFERENCES `categoria` (`id_genero`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
