<?php
class PelisModel
{

function __construct()
{

}
    function mostrarTodasPeliculas()
    {
        $db = new PDO('mysql:host=localhost;' . 'dbname=barba_negra;charset=utf8', 'root', '');

        $query = $db->prepare('SELECT * FROM pelicula');
        $query->execute();

        $peliculas = $query->fetchAll(PDO::FETCH_OBJ);

        return $peliculas;
    }
    function obtenerPeliculasXGenero($genero)
    {
        $db = new PDO('mysql:host=localhost;' . 'dbname=barba_negra;charset=utf8', 'root', '');

        $query = $db->prepare('SELECT * FROM `pelicula` WHERE genero_id = ?');
        $query->execute([$genero]);

        $peliculasXGenero = $query->fetchAll(PDO::FETCH_OBJ);

        return $peliculasXGenero;
    }

    function agregarPelicula($nombre, $duracion, $anio, $genero)
    {
        $db = new PDO('mysql:host=localhost;' . 'dbname=barba_negra;charset=utf8', 'root', '');
        $query = $db->prepare('INSERT INTO `pelicula`( `nombre`, `duracion`, `anio`, `genero_id`) VALUES (?,?,?,?)');
        $query->execute([$nombre, $duracion, $anio, $genero]);
    }

    function eliminarPelicula($id)
    {

        $db = new PDO('mysql:host=localhost;' . 'dbname=barba_negra;charset=utf8', 'root', '');
        $query = $db->prepare('DELETE FROM `pelicula` WHERE id_pelicula = ?');
        $query->execute([$id]);
    }

    function mostrarPeliculasXCalif($calificacion)
    {
        $db = new PDO('mysql:host=localhost;' . 'dbname=barba_negra;charset=utf8', 'root', '');

        $query = $db->prepare('SELECT * FROM `pelicula` WHERE calificacion = ?');
        $query->execute([$calificacion]);

        $peliculasXCalif = $query->fetchAll(PDO::FETCH_OBJ);

        return $peliculasXCalif;
    }

    function obtenerPeliAEditar($id)
    {
        $db = new PDO('mysql:host=localhost;' . 'dbname=barba_negra;charset=utf8', 'root', '');
        $query = $db->prepare('SELECT * FROM `pelicula` WHERE id_pelicula = ?');
        $query->execute([$id]);

        $peliAEditar = $query->fetch(PDO::FETCH_OBJ);

        return $peliAEditar;
    }

    function edicionConfirmada($id,$nombre,$duracion,$anio,$genero){
        $db = new PDO('mysql:host=localhost;' . 'dbname=barba_negra;charset=utf8', 'root', '');
        $query = $db->prepare('UPDATE `pelicula` SET `nombre`= ? ,`duracion`=?,`anio`=?,`genero_id`=? WHERE id_pelicula = ?');
        $query->execute([$nombre,$duracion,$anio,$genero,$id]);

    }

    function agregarNuevoGenero($nombre){
        $db = new PDO('mysql:host=localhost;' . 'dbname=barba_negra;charset=utf8', 'root', '');
        $query = $db->prepare('INSERT INTO `categoria`(`nombre`) VALUES (?)');
        $query->execute([$nombre]);
    }
    function obtenerGeneros(){
        $db = new PDO('mysql:host=localhost;' . 'dbname=barba_negra;charset=utf8', 'root', '');
        $query = $db->prepare('SELECT * FROM `categoria`');
        $query->execute();

        $generos = $query->fetchAll(PDO::FETCH_OBJ);
        return $generos;
    }
    function generoEliminado($id){
        $db = new PDO('mysql:host=localhost;' . 'dbname=barba_negra;charset=utf8', 'root', '');
        $query = $db->prepare('DELETE FROM `categoria` WHERE id_genero = ?');
        $query->execute([$id]);

    }
}
