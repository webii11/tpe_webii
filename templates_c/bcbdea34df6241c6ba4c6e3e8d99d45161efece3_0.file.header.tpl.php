<?php
/* Smarty version 3.1.39, created on 2021-10-15 01:29:34
  from 'C:\xampp\htdocs\WEB II\TPE_WEBII\templates\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6168bd5e750191_73077773',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bcbdea34df6241c6ba4c6e3e8d99d45161efece3' => 
    array (
      0 => 'C:\\xampp\\htdocs\\WEB II\\TPE_WEBII\\templates\\header.tpl',
      1 => 1634254172,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6168bd5e750191_73077773 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">

<head>
    
    <base href="<?php echo BASE_URL;?>
">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pelis Barba Negra</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="inicio">Barba Negra</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="inicio">Inicio</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Generos
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="genero/1">Accion</a></li>
                            <li><a class="dropdown-item" href="genero/2">Comedia</a></li>
                            <li><a class="dropdown-item" href="genero/5">Terror</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="agregarGenero">Agregar Genero</a></li>
                            <li><a class="dropdown-item" href="borrarGenero">Borrar Genero</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            Calificacion
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="calificacion/1">1 Estrella</a></li>
                            <li><a class="dropdown-item" href="calificacion/2">2 Estrellas</a></li>
                            <li><a class="dropdown-item" href="calificacion/3">3 Estrellas</a></li>
                            <li><a class="dropdown-item" href="calificacion/4">4 Estrellas</a></li>
                            <li><a class="dropdown-item" href="calificacion/5">5 Estrellas</a></li>
                        </ul>
                    </li>
                </ul>
                <li class="nav-item">
                    <a class="nav-link" id="ingresar " href="#">Ingresar</a>
                </li>
            </div>
        </div>
    </nav>
    <div>



    <?php }
}
