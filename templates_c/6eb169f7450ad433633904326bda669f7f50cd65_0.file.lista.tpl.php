<?php
/* Smarty version 3.1.39, created on 2021-10-15 01:26:18
  from 'C:\xampp\htdocs\WEB II\TPE_WEBII\templates\lista.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_6168bc9a213b45_36491501',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6eb169f7450ad433633904326bda669f7f50cd65' => 
    array (
      0 => 'C:\\xampp\\htdocs\\WEB II\\TPE_WEBII\\templates\\lista.tpl',
      1 => 1634253856,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:form_agregar.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_6168bc9a213b45_36491501 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<h1>Peliculas</h1>
<?php $_smarty_tpl->_subTemplateRender('file:form_agregar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<table>
    <thead>
        <tr>
            <td>Nombre</td>
            <td>Calificación</td>
            <td>Duración</td>
            <td>Genero</td>
        </tr>
    </thead>
    <tbody>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['peliculas']->value, 'pelicula');
$_smarty_tpl->tpl_vars['pelicula']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['pelicula']->value) {
$_smarty_tpl->tpl_vars['pelicula']->do_else = false;
?>
        <tr>
            <td> <?php echo $_smarty_tpl->tpl_vars['pelicula']->value->nombre;?>
 </td>
            <td> <?php echo $_smarty_tpl->tpl_vars['pelicula']->value->calificacion;?>
 </td>
            <td> <?php echo $_smarty_tpl->tpl_vars['pelicula']->value->duracion;?>
 </td>
            <td> <?php echo $_smarty_tpl->tpl_vars['pelicula']->value->genero_id;?>
 </td>
            <td><a href="borrar/<?php echo $_smarty_tpl->tpl_vars['pelicula']->value->id_pelicula;?>
" ><button>Borrar</button></a></td>
            <td><a href="editar/<?php echo $_smarty_tpl->tpl_vars['pelicula']->value->id_pelicula;?>
" ><button>Editar</button></a></td>
        </tr>
    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </tbody
</table>

<?php $_smarty_tpl->_subTemplateRender('file:footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
