<?php

require_once('controllers/controller.php');


define('BASE_URL', '//' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']) . '/');

// leo accion y params
if (!empty($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = 'inicio';
}

$controller = new Controller();

// camino a seguir en cada opcion seleccionada
$params = explode('/', $action);
switch ($params[0]) {

    case 'inicio':
       $controller->mostrarPeliculas();
        break;

    case 'genero':
        $controller->mostrarGenero($params[1]);
        break;

    case 'calificacion':
        $controller->mostrarCalificacion($params[1]);
        break;

    case 'insertar':
        $controller->agregarPeli();
        break;
    case 'borrar':
        $controller->borrarPeli($params[1]);
        break;

    case 'editar':
        $controller->editarPeli($params[1]);
        break;

    case 'confirmacionEdicion':
        $controller->confirmarEdicion($params[1]);
        break;

    case 'agregarGenero':
        $controller->agregarGenero();
        break;

    case 'confirmarAgregarGenero':
        $controller->confirmaGeneroNuevo();
        break;

    case 'borrarGenero':
        $controller->borrarGenero();
        break;
    case 'eliminarGenero':
        $controller->eliminarGenero($params[1]);
        break;

        default:
        echo ('404 Page not found');
        break;
}
