{include file="header.tpl" }

<h1>Generos</h1>
<table>
    <thead>
        <tr>
            <td>Nombre</td>
        </tr>
    </thead>
    <tbody>
    {foreach from=$generos item=genero}
        <tr>
            <td> {$genero->nombre} </td>

            <td><a href="eliminarGenero/{$genero->id_genero}" ><button>Borrar</button></a></td>
        </tr>
    {/foreach}
    </tbody
</table>

{include file="footer.tpl" }