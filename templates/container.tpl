{include 'header.tpl'}

        <div class="row align-items-start">
            <div class="col">
                <div class="card-trip">
                    <img src="https://raw.githubusercontent.com/lewagon/fullstack-images/master/uikit/greece.jpg" />
                    <div class="card-trip-infos">
                        <div>
                            <h2>Title here</h2>
                            <p>Short description here!</p>
                        </div>
                        <h2 class="card-trip-pricing">£99.99</h2>
                    </div>
                </div>

            </div>
            <div class="col">
                <div class="card-trip">
                    <img src="https://raw.githubusercontent.com/lewagon/fullstack-images/master/uikit/greece.jpg" />
                    <div class="card-trip-infos">
                        <div>
                            <h2>Title here</h2>
                            <p>Short description here!</p>
                        </div>
                        <h2 class="card-trip-pricing">£99.99</h2>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card-trip">
                    <img src="https://raw.githubusercontent.com/lewagon/fullstack-images/master/uikit/greece.jpg" />
                    <div class="card-trip-infos">
                        <div>
                            <h2>Title here</h2>
                            <p>Short description here!</p>
                        </div>
                        <h2 class="card-trip-pricing">£99.99</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row align-items-center">

            <div class="col">
                <div class="card-trip">
                    <img src="https://raw.githubusercontent.com/lewagon/fullstack-images/master/uikit/greece.jpg" />
                    <div class="card-trip-infos">
                        <div>
                            <h2>Title here</h2>
                            <p>Short description here!</p>
                        </div>
                        <h2 class="card-trip-pricing">£99.99</h2>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card-trip">
                    <img src="https://raw.githubusercontent.com/lewagon/fullstack-images/master/uikit/greece.jpg" />
                    <div class="card-trip-infos">
                        <div>
                            <h2>Title here</h2>
                            <p>Short description here!</p>
                        </div>
                        <h2 class="card-trip-pricing">£99.99</h2>
                    </div>
                </div>
            </div>
            <div class="col">
                One of three columns
            </div>

{include file='footer.tpl'}