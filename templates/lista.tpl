{include file="header.tpl"}

<h1>Peliculas</h1>
{include file='form_agregar.tpl'}
<table>
    <thead>
        <tr>
            <td>Nombre</td>
            <td>Calificación</td>
            <td>Duración</td>
            <td>Genero</td>
        </tr>
    </thead>
    <tbody>
    {foreach from=$peliculas item=pelicula}
        <tr>
            <td> {$pelicula->nombre} </td>
            <td> {$pelicula->calificacion} </td>
            <td> {$pelicula->duracion} </td>
            <td> {$pelicula->genero_id} </td>
            <td><a href="borrar/{$pelicula->id_pelicula}" ><button>Borrar</button></a></td>
            <td><a href="editar/{$pelicula->id_pelicula}" ><button>Editar</button></a></td>
        </tr>
    {/foreach}
    </tbody
</table>

{include file='footer.tpl'}